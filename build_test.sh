#!/bin/bash

mkdir tests/bin &> /dev/null;
rm tests/bin/* &> /dev/null;

PASSES=0;
FAILS=0;

echo "";

for i in `ls tests/src/*.asm`; do
	echo -n "Building $i: [";
	o=${i/src/bin/}
	o=${o/.asm/}
	./bin/assembler $i > $o.txt;
	mv out $o.test.bin &> /dev/null;

	if [ -f "$o.test.bin" ]; then
		nasm -fbin -o $o.nasm.bin $i;
		if [ $(sha1sum $o.test.bin | cut -d\  -f1) == $(sha1sum $o.nasm.bin | cut -d\  -f1) ]; then
			echo -e " \e[92mPass\e[0m ]";
			((PASSES++));
		else
			echo -e "\e[97;41;1m FAIL \e[0m]";
			((FAILS++));
		fi
	else
		echo -e "\e[97;41;1m FAIL \e[0m]";
		((FAILS++));
	fi
done

echo -e "\nPasses: $PASSES\nFails: $FAILS";
