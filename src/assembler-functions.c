void ASSEMBLER_parse_number(struct ASSEMBLER_line *line, uint64_t symbol_index){
	static struct ASSEMBLER_symbol* symbol;
	static char *token;
	static uint64_t token_length;

	token = line->symbols[symbol_index].text;
	token_length = strlen(token);

	symbol = &line->symbols[symbol_index];
	symbol->value = NULL;
	symbol->value_length = 0;

	switch(token[0]){
		case '0':
			if(token[1] == 'x' || token[1] == 'X'){
				token += 2;
				token_length -= 2;

				if(token_length % 2)
					ASSEMBLER_generate_error(line, ERROR_ERROR, "Hex values must have an even number of digits!", 0, 0);

				static char* value;
				symbol->value_length = token_length/2;
				symbol->value = malloc(symbol->value_length);
				memset(symbol->value, 0, symbol->value_length);

				value = (char*)symbol->value + symbol->value_length - 1;
				for(;;){
					if(token[0] >= '0' && token[0] <= '9')			value[0] ^= token[0] - 0x30;
					else if(token[0] >= 'A' && token[0] <= 'F')		value[0] ^= token[0] - 0x37;
					else if(token[0] >= 'a' && token[0] <= 'f')		value[0] ^= token[0] - 0x57;
					else
						ASSEMBLER_generate_error(line, ERROR_ERROR, "Not a valid hex character!", 0, 0);

					token++;
					token_length--;

					if(token_length % 2)
						value[0] <<= 4;
					else
						value--;

					if(!token_length)
						break;
				}

				return;
			}else if(token[1] == 'b' || token[1] == 'B'){
				token += 2;
				token_length -= 2;

				if(token_length % 8)
					ASSEMBLER_generate_error(line, ERROR_ERROR, "Binary values must have a number of digits divisable by 8!", 0, 0);

				symbol->value_length = token_length/8;
				symbol->value = malloc(symbol->value_length);
				memset(symbol->value, 0, symbol->value_length);

				for(;;){
					symbol->value = (void*)((uint64_t)symbol->value ^ (token[0] - 0x30));
					token_length--;
					if(!token_length)
						break;

					symbol->value = (void*)((uint64_t)symbol->value << 1);
				}

				return;
			}
			// fallthrough
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			// How to accurately get value_length now? I just do it later for now
			symbol->value_length = token_length;
			symbol->value = malloc(symbol->value_length);
			memset(symbol->value, 0, symbol->value_length);

			for(;;){
				if(token[0] >= '0' && token[0] <= '9')
					symbol->value = (void*)((uint64_t)symbol->value * (token[0] - 0x30));
				else{
					ASSEMBLER_generate_error(line, ERROR_ERROR, "Invalid Digit!", 0, 0);
				}

				token_length--;
				if(!token_length)
					break;

				symbol->value = (void*)((uint64_t)symbol->value * 10);
				token++;
			}

			for(;token_length;token_length--)
				if((uint64_t)symbol->value + (token_length * sizeof(char*)))
					break;

			if(!token_length){
				token_length = 1;
				symbol->value = 0;
			}

			if(symbol->value_length != token_length){
				symbol->value_length = token_length;
				symbol->value = realloc(symbol->value, symbol->value_length);

				if(symbol->value == NULL){
					printf("REALLOC FAILED!\n");
					exit(0);
				}
			}

			return;
		break;
	}
}



void ASSEMBLER_generate_error(struct ASSEMBLER_line *line, enum ASSEMBLER_error_types error_type, char *error_string, uint64_t start, uint64_t end){
	static struct ASSEMBLER_error *error;

	error = malloc(sizeof(struct ASSEMBLER_error));
	error->error_type = error_type;
	error->error_string = error_string;
	error->start = start;
	error->end = end;

	error->next = line->errors;
	line->errors = error;
}



void ASSEMBLER_architecture_directive_handler(struct ASSEMBLER_line *line){
	if(line->symbols_count == 1)
		ASSEMBLER_generate_error(line, ERROR_ERROR, "Missing argument", 0, 0);
	else{
		if(line->symbols_count > 2)
			ASSEMBLER_generate_error(line, ERROR_WARNING, "Only takes one argument", 0, 0);

		static uint8_t counter, new_architecture;
		for(counter=0; counter < ASSEMBLER_ARCHITECTURE_COUNT; counter++)
			if(!strcasecmp(line->symbols[1].text, ASSEMBLER_ARCHITECTURES[counter].text)){
				new_architecture = counter;
				break;
			}

		if(counter == ASSEMBLER_ARCHITECTURE_COUNT)
			ASSEMBLER_generate_error(line, ERROR_ERROR, "Unknown architecture", 0, 0);
		else
			current_architecture = new_architecture;
	}
}

void ASSEMBLER_base_address_directive_handler(struct ASSEMBLER_line *line){
	if(line->symbols_count == 1)
		ASSEMBLER_generate_error(line, ERROR_ERROR, "Missing argument", 0, 0);
	else{
		if(line->symbols_count > 2)
			ASSEMBLER_generate_error(line, ERROR_WARNING, "Only takes one argument", 0, 0);

		ASSEMBLER_parse_number(line, 1);

		if(!line->symbols[1].value_length)
			ASSEMBLER_generate_error(line, ERROR_WARNING, "Argument is not a number?", 0, 0);
		else{
			if(line->symbols[1].value_length > 8)
				line->symbols[1].value_length = 8;

			memcpy(&base_memory_address, line->symbols[1].value, line->symbols[1].value_length);
			free(line->symbols[1].value);
			line->symbols[1].value_length = 0;
		}
	}
}

void ASSEMBLER_declare_directive_handler(struct ASSEMBLER_line *line){
	line = line;
}

void ASSEMBLER_output_filename_directive_handler(struct ASSEMBLER_line *line){
	if(line->symbols_count == 1)
		ASSEMBLER_generate_error(line, ERROR_ERROR, "Missing argument", 0, 0);
	else{
		if(line->symbols_count > 2)
			ASSEMBLER_generate_error(line, ERROR_WARNING, "Only takes one argument", 0, 0);

		output_file_name = line->symbols[1].text;
	}

}

void ASSEMBLER_output_format_directive_handler(struct ASSEMBLER_line *line){
	if(line->symbols_count == 1)
		ASSEMBLER_generate_error(line, ERROR_ERROR, "Missing argument", 0, 0);
	else{
		if(line->symbols_count > 2)
			ASSEMBLER_generate_error(line, ERROR_WARNING, "Only takes one argument", 0, 0);

		static uint8_t counter, new_output_format;
		for(counter=0; counter < ASSEMBLER_OUTPUT_FORMAT_COUNT; counter++)
			if(!strcasecmp(line->symbols[1].text, ASSEMBLER_OUTPUT_FORMATS[counter].text)){
				new_output_format = counter;
				break;
			}

		if(counter == ASSEMBLER_OUTPUT_FORMAT_COUNT)
			ASSEMBLER_generate_error(line, ERROR_ERROR, "Unknown output format", 0, 0);
		else
			current_output_format = new_output_format;
	}
}

void ASSEMBLER_reserve_directive_handler(struct ASSEMBLER_line *line){
	line = line;
}
