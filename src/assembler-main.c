#define _DEFAULT_SOURCE

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>



// Includes

#include "assembler.h"



#if ASSEMBLER_CONFIGURATION_ARCHITECTURE_X86_64
#include "architectures/x86_64/main.h"
#endif



#if ASSEMBLER_CONFIGURATION_OUTPUT_FORMAT_FLAT_BINARY
#include "output-formats/flat-binary.h"
#endif




// Declare stuff

const char* ASSEMBLER_ERROR_TYPE_STRINGS[] = {
	"WARNING",
	"ERROR"
};

uint64_t base_memory_address = 0;
uint64_t current_architecture = 0; // Defaults to the first available architecture (x86_64?)
uint64_t current_output_format = 0; // Defaults to the first available output format (flat binary?)
char *output_file_name = "a.out";


const uint8_t ASSEMBLER_ARCHITECTURE_COUNT = ASSEMBLER_CONFIGURATION_ARCHITECTURE_X86_64 /* add all arch defines */;
const struct ASSEMBLER_architecture_callbacks ASSEMBLER_ARCHITECTURES[] = {
#if ASSEMBLER_CONFIGURATION_ARCHITECTURE_X86_64
	{
		"X86_64",
		x86_64_line_handler,
		1,
		(struct ASSEMBLER_generic_callbacks*)&X86_64_DIRECTIVES
	},
#endif
};


const uint8_t ASSEMBLER_OUTPUT_FORMAT_COUNT = ASSEMBLER_CONFIGURATION_OUTPUT_FORMAT_FLAT_BINARY /* add all ouput format defines */;
const struct ASSEMBLER_output_format_callbacks ASSEMBLER_OUTPUT_FORMATS[] = {
#if ASSEMBLER_CONFIGURATION_OUTPUT_FORMAT_FLAT_BINARY
	{"FLAT_BINARY", flat_binary_line_handler},
#endif
};


#include "assembler-functions.c"


const uint8_t ASSEMBLER_DIRECTIVES_COUNT = 6;
const struct ASSEMBLER_generic_callbacks ASSEMBLER_DIRECTIVES[] = {
	{"ARCHITECTURE", ASSEMBLER_architecture_directive_handler},
	{"BASE_ADDRESS", ASSEMBLER_base_address_directive_handler},
	{"DECLARE", ASSEMBLER_declare_directive_handler},
	{"OUTPUT_FILENAME", ASSEMBLER_output_filename_directive_handler},
	{"OUTPUT_FORMAT", ASSEMBLER_output_format_directive_handler},
	{"RESERVE", ASSEMBLER_reserve_directive_handler},
};


#if ASSEMBLER_CONFIGURATION_MISC_NASM_COMPATIBILITY
#include "assembler-nasm-compatibility.c"

const uint8_t ASSEMBLER_NASM_COMPATIBLE_COMMANDS_COUNT = 1;
const struct ASSEMBLER_generic_callbacks ASSEMBLER_NASM_COMPATIBLE_COMMANDS[] = {
	{"ORG", ASSEMBLER_base_address_directive_handler}
};
#endif

// Main

int main(int argc, char *argv[]){
	if(argc == 1){
		printf("USAGE:\n\n\t%s file.asm\n\n", argv[0]);
		return 0;
	}

	if(argc > 2){
		printf("You may only specify one file!\n\n");
		return 0;
	}

	FILE *in = fopen(argv[1], "r"); 
	if(in == NULL){
		printf("Unable to open file \"%s\"\n", argv[1]);
		return 0;
	}

	fseek(in, 0, SEEK_END);
	long file_length = ftell(in);
	if(file_length  < 1){
		printf("Unable to get file length: \"%s\"\n", argv[1]);
		return 0;
	}

	fseek(in, 0, SEEK_SET);
	char *file_contents = malloc(file_length+1);
	char *orig_file_contents = file_contents;
	size_t read_file_length = fread(file_contents, 1, file_length, in);
	if((long)read_file_length != (long)file_length){
		printf("Unable to read entire file! %"PRIu64"/%"PRIu64, read_file_length, file_length);
		goto ASSEMBLER_cleanup;
		return 0;
	}

	file_contents[file_length] = '\0';
	fclose(in);

	struct ASSEMBLER_line *lines_root = NULL, **new_line_pointer = &lines_root;
	while(file_contents != NULL){
		static struct ASSEMBLER_line *this_line;
		static char *comment, *this_line_str = NULL;
		static uint64_t line_length;
		static uint64_t line_number = 1;

		this_line = malloc(sizeof(struct ASSEMBLER_line));
		this_line->line_number = line_number;
		this_line->errors = NULL;
		this_line->next = NULL;
		line_number++;

		this_line_str = strsep(&file_contents, "\r\n");
		if(this_line_str == NULL){
			free(this_line);
			continue;
		}

		line_length = strlen(this_line_str);
		if(!line_length){
			free(this_line);
			continue;
		}

		comment = strchr(this_line_str, ';');
		if(comment != NULL){
			if(comment == this_line_str){
				if(comment[1] == ' '){
					free(this_line);
					continue;
				}else
					comment++;
			}else{
				comment[0] = '\0';
				comment = NULL;
			}
		}

		this_line->line_string = malloc(strlen(this_line_str)+1);
		strcpy(this_line->line_string, this_line_str);

		this_line->symbols = malloc((line_length/2) * sizeof(struct ASSEMBLER_symbol));
		this_line->symbols_count = 0;
		for(;;){
			this_line->symbols[this_line->symbols_count].text = strsep(&this_line_str, " ,\t");
			this_line->symbols[this_line->symbols_count].value_length = 0;

			while(
				this_line->symbols[this_line->symbols_count].text[0] == ' '
			 || this_line->symbols[this_line->symbols_count].text[0] == ','
			 || this_line->symbols[this_line->symbols_count].text[0] == '\t'
			)
				this_line->symbols[this_line->symbols_count].text++;

			if(strlen(this_line->symbols[this_line->symbols_count].text))
				this_line->symbols_count++;

			if(this_line_str == NULL)
				break;
		}

		if(!this_line->symbols_count){
			free(this_line->line_string);
			free(this_line->symbols);
			free(this_line);
			continue;
		}

		this_line->symbols = realloc(this_line->symbols, this_line->symbols_count * sizeof(struct ASSEMBLER_symbol));
		if(this_line->symbols == NULL){
			printf("TOKEN REALLOC FAILED!\n");
			exit(0);
		}

		if(comment == NULL){
#if ASSEMBLER_CONFIGURATION_MISC_NASM_COMPATIBILITY
			static uint8_t counter;
			for(counter=0; counter<ASSEMBLER_NASM_COMPATIBLE_COMMANDS_COUNT; counter++)
				if(!strcasecmp(ASSEMBLER_NASM_COMPATIBLE_COMMANDS[counter].text, this_line->symbols[0].text)){
					if(ASSEMBLER_NASM_COMPATIBLE_COMMANDS[counter].line_handler == NULL)
						ASSEMBLER_generate_error(this_line, ERROR_WARNING, "NASM compatibility not yet implemented", 0, 0);
					else
						ASSEMBLER_NASM_COMPATIBLE_COMMANDS[counter].line_handler(this_line);

					break;
				}

			if(counter == ASSEMBLER_NASM_COMPATIBLE_COMMANDS_COUNT){
#endif

			if(ASSEMBLER_ARCHITECTURES[current_architecture].line_handler != NULL)
				ASSEMBLER_ARCHITECTURES[current_architecture].line_handler(this_line);
			else
				ASSEMBLER_generate_error(this_line, ERROR_WARNING, "Architeture line handler not yet implemented", 0, 0);

#if ASSEMBLER_CONFIGURATION_MISC_NASM_COMPATIBILITY
			}
#endif
		}else{
			static uint8_t counter, success;

			success = 0;
			for(counter=0; counter<ASSEMBLER_DIRECTIVES_COUNT; counter++)
				if(!strcasecmp(ASSEMBLER_DIRECTIVES[counter].text, comment)){
					if(ASSEMBLER_DIRECTIVES[counter].line_handler == NULL)
						ASSEMBLER_generate_error(this_line, ERROR_WARNING, "Directive not yet implemented", 0, 0);
					else{
						ASSEMBLER_DIRECTIVES[counter].line_handler(this_line);
						success = 1;
					}

					break;
				}

			if(!success && ASSEMBLER_ARCHITECTURES[current_architecture].directives_count){
				for(counter=0; counter<ASSEMBLER_ARCHITECTURES[current_architecture].directives_count; counter++)
					if(!strcasecmp(ASSEMBLER_ARCHITECTURES[current_architecture].directives[counter].text, comment)){
						if(ASSEMBLER_ARCHITECTURES[current_architecture].directives[counter].line_handler == NULL)
							ASSEMBLER_generate_error(this_line, ERROR_WARNING, "Directive not yet implemented", 0, 0);
						else{
							ASSEMBLER_ARCHITECTURES[current_architecture].directives[counter].line_handler(this_line);
							success = 1;
						}

						break;
					}
			}

			if(!success)
				ASSEMBLER_generate_error(this_line, ERROR_WARNING, "Unknown directive, put a space after ; if this is a comment", 0, 0);
		}

		*new_line_pointer = this_line;
		new_line_pointer = &(this_line->next);
	}

	uint64_t warning_count = 0;
	uint64_t error_count = 0;

	struct ASSEMBLER_line* line = lines_root;
	while(line != NULL){
		static struct ASSEMBLER_error *line_errors;
		line_errors = line->errors;

		while(line_errors != NULL){
			fprintf(stderr, "%"PRIu64" | %s - %s:\n", line->line_number, ASSEMBLER_ERROR_TYPE_STRINGS[line_errors->error_type], line_errors->error_string);

			switch(line_errors->error_type){
				case ERROR_ERROR:
					error_count++;
				break;

				case ERROR_WARNING:
					warning_count++;
				break;
			}

			line_errors = line_errors->next;
		}

		if(line->errors != NULL)
			fprintf(stderr, "\t%s\n\n", line->line_string);

		line = line->next;
	}

	if(warning_count)
		fprintf(stderr, "Warnings: %"PRIu64"\t", warning_count);

	if(error_count)
		fprintf(stderr, "Errors: %"PRIu64, error_count);

	if(warning_count || error_count)
		fprintf(stderr, "\n");

	if(!error_count){
		if(ASSEMBLER_OUTPUT_FORMATS[current_output_format].line_handler == NULL){
			printf("Assembly successful, output format %s not yet implemented\n\nexiting now\n", ASSEMBLER_OUTPUT_FORMATS[current_output_format].text);
			goto ASSEMBLER_cleanup;
		}

		FILE *out = fopen(output_file_name, "w");
		if(out == NULL){
			printf("Unable to open output file?");
			return 0;
		}

		if(ASSEMBLER_OUTPUT_FORMATS[current_output_format].line_handler(lines_root, out))
			fclose(out);
		else{
			fprintf(stderr, "Unable to write all data to output file\n");
			unlink(output_file_name);
			return 0;
		}
	}


	// Clean up

	ASSEMBLER_cleanup:
	while(lines_root != NULL){
		static struct ASSEMBLER_error *errors;
		static struct ASSEMBLER_line *line;

		errors = lines_root->errors;
		while(errors != NULL){
			static struct ASSEMBLER_error *error;

			error = errors;
			errors = errors->next;
			free(error);
		}

		line = lines_root;
		lines_root = lines_root->next;

		if(line->symbols_count)
			do{
				line->symbols_count--;
				if(line->symbols[line->symbols_count].value_length)
					free(line->symbols[line->symbols_count].value);
			}while(line->symbols_count);

		free(line->line_string);
		free(line->symbols);
		free(line);
	}

	free(orig_file_contents);

	return 0;
}
