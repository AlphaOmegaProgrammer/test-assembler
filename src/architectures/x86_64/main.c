#include <inttypes.h>

#include <string.h>
#include <strings.h>
#include <stdlib.h> 



// Includes

#include "assembler.h"

#include "architectures/x86_64/64_bit/main.h"



// Declare stuff

uint8_t current_cpu_mode = 1;
const uint8_t X86_64_CPU_MODE_COUNT = 2;
const char *X86_64_CPU_MODES[] = {
	"NONE",
	"64-BIT"/*,
	"PROTECTED",
	"VIRTUAL-8086",
	"REAL",*/
};



// "main"

void x86_64_cpu_mode_directive_handler(struct ASSEMBLER_line* line){
	static uint8_t counter, new_cpu_mode;
	for(counter=0, new_cpu_mode=0; counter<X86_64_CPU_MODE_COUNT; counter++)
		if(!strcasecmp(line->symbols[1].text, X86_64_CPU_MODES[counter])){
			new_cpu_mode = counter;
			break;
		}

	if(new_cpu_mode)
		current_cpu_mode = new_cpu_mode;
	else
		ASSEMBLER_generate_error(line, ERROR_ERROR, "x86_64 - Invalid CPU Mode", 0, 0);
}

void x86_64_line_handler(struct ASSEMBLER_line *line){
	if(!strcmp(X86_64_CPU_MODES[current_cpu_mode], "64-BIT"))
		assemble_line_x86_64_64_bit(line);
	else
		ASSEMBLER_generate_error(line, ERROR_ERROR, "x86_64 - Unknown or unimplemented CPU mode?", 0, 0);
}
