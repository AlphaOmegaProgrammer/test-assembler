#ifndef ARCHITECTURE_x86_64_64_BIT_MODE_INCLUDED
#define ARCHITECTURE_x86_64_64_BIT_MODE_INCLUDED

#include "assembler.h"

struct X86_64_64_BIT_line_extra_data{
	uint8_t operand_size_override:1;
	uint8_t address_size_override:1;
	uint8_t segment_override:1;
	uint8_t lock:1;
	uint8_t repeat:1;
};


void assemble_line_x86_64_64_bit(struct ASSEMBLER_line*);

#endif /* ARCHITECTURE_x86_64_64_BIT_MODE_INCLUDED */
