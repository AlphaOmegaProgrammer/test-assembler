#include "../main.h"

void X86_64_64_BIT_INSTRUCTION_FUNCTION_MOV(struct ASSEMBLER_line*, uint64_t);
void X86_64_64_BIT_INSTRUCTION_FUNCTION_NOP(struct ASSEMBLER_line*, uint64_t);
