#include <stdlib.h>

#include "assembler.h"

#include "../main.h"

void X86_64_64_BIT_INSTRUCTION_FUNCTION_NOP(struct ASSEMBLER_line *line, uint64_t symbol_index){
	static unsigned char *value;

	line->symbols[symbol_index].value = malloc(1);
	value = line->symbols[symbol_index].value;
	value[0] = 0x90;
	line->symbols[symbol_index].value_length = 1;
}
