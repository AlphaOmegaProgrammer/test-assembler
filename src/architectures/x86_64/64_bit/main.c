#include <string.h>
#include <strings.h>

#include "assembler.h"

#include "instructions/64_bit_all_instruction_functions.h"


// Declare stuff

const uint16_t X86_64_64_BIT_INSTRUCTIONS_COUNT = 267;
const char *X86_64_64_BIT_INSTRUCTIONS[] = {
	"ADC",
	"ADCX",
	"ADD",
	"ADOX",
	"AND",
	"ANDN",
	"BEXTR",
	"BLCFILL",
	"BLCI",
	"BLCIC",
	"BLCMSK",
	"BLCS",
	"BLSFILL",
	"BLSL",
	"BLSIC",
	"BLSMSK",
	"BLSR",
	"BSF",
	"BSR",
	"BSWAP",
	"BT",
	"BTC",
	"BTR",
	"BTS",
	"BZHI",
	"CALL",
	"CBW",
	"CDQ",
	"CDQE",
	"CWDE",
	"CQO",
	"CWD",
	"CLAC",
	"CLC",
	"CLD",
	"CLFLUSH",
	"CLFLUSHOPT",
	"CLGI",
	"CLI",
	"CLTS",
	"CLZERO",
	"CMC",
	"CMOV",
	"CMP",
	"CMPS",
	"CMPSB",
	"CMPSW",
	"CMPSD",
	"CMPSQ",
	"CMPXCHG",
	"CMPXCHG8B",
	"CMOXCHG16B",
	"CPUID",
	"CRC32",
	"DEC",
	"DIV",
	"ENTER",
	"HLT",
	"IDIV",
	"IMUL",
	"IN",
	"INC",
	"INS",
	"INSB",
	"INSW",
	"INSD",
	"INT",
	"INT 3",
	"INVD",
	"INVLPG",
	"INVLPGA",
	"IRET",
	"IRETD",
	"IRETQ",
	"JA",
	"JAE",
	"JB",
	"JBE",
	"JC",
	"JCXZ",
	"JE",
	"JECXZ",
	"JG",
	"JGE",
	"JL",
	"JO",
	"JS",
	"JLE",
	"JMP",
	"JNA",
	"JNAE",
	"JNB",
	"JNBE",
	"JNC",
	"JNE",
	"JNG",
	"JNGE",
	"JNL",
	"JNLE",
	"JNO",
	"JNP",
	"JNS",
	"JNZ",
	"JP",
	"JPE",
	"JPO",
	"JRCXZ",
	"JZ",
	"LAHF",
	"LAR",
	"LDS",
	"LES",
	"LFS",
	"LGDT",
	"LIDT",
	"LLDT",
	"LGS",
	"LSS",
	"LEA",
	"LEAVE",
	"LFENCE",
	"LLWPCB",
	"LMSW",
	"LODS",
	"LODSB",
	"LODSW",
	"LODSD",
	"LODSQ",
	"LOOP",
	"LOOPE",
	"LOOPNE",
	"LOOPNZ",
	"LOOPZ",
	"LSL",
	"LTR",
	"LWPINS",
	"LWPVAL",
	"LZCNT",
	"MFENCE",
	"MONITOR",
	"MONITORX",
	"MOV",
	"MOVBE",
	"MOVD",
	"MOVMSKPD",
	"MOVMSKPS",
	"MOVNTI",
	"MOVS",
	"MOVSB",
	"MOVSD",
	"MOVSQ",
	"MOVSW",
	"MOVSX",
	"MOVSXD",
	"MOVZX",
	"MUL",
	"MULX",
	"MWAIT",
	"MWAITX",
	"NEG",
	"NOP",
	"NOT",
	"OR",
	"OUT",
	"OUTS",
	"OUTSB",
	"OUTSD",
	"OUTSW",
	"PAUSE",
	"PDEP",
	"PEXT",
	"POP",
	"POPA",
	"POPAD",
	"POPCNT",
	"POPF",
	"POPFD",
	"MOMFQ",
	"PREFETCH",
	"PREFETCHW",
	"PREFETCHNTA",
	"PREFETCHT0",
	"PREFETCHT1",
	"PREFETCHT2",
	"PUSH",
	"PUSHF",
	"PUSHFD",
	"PUSHFQ",
	"RCL",
	"RCR",
	"RDFSBASE",
	"RDGSBASE",
	"RDMSR",
	"RDPMC",
	"RDRAND",
	"RDSEED",
	"RDTSC",
	"RDTSCP",
	"RET",
	"ROL",
	"ROR",
	"RORX",
	"RSM",
	"SAHF",
	"SAL",
	"SHL",
	"SAR",
	"SARX",
	"SBB",
	"SCAS",
	"SCASB",
	"SCASW",
	"SCASD",
	"SCASQ",
	"SET",
	"SFENCE",
	"SGDT",
	"SIDT",
	"SKINIT",
	"SHL",
	"SHLD",
	"SHLX",
	"SHR",
	"SHRD",
	"SHRX",
	"SLDT",
	"SLWPCB",
	"SMSW",
	"STAC",
	"STC",
	"STD",
	"STI",
	"STGI",
	"STOS",
	"STOSB",
	"STOSW",
	"STOSD",
	"STOSQ",
	"STR",
	"SUB",
	"SWAPGS",
	"SYSCALL",
	"SYSENTER",
	"SYSEXIT",
	"SYSRET",
	"TIMSKC",
	"TEST",
	"TZCNT",
	"TZMSK",
	"UD0",
	"UD1",
	"UD2",
	"VERR",
	"VERW",
	"VMLOAD",
	"VMMCALL",
	"VMRUN",
	"VMSAVE",
	"WBINVC",
	"WRFSBASE",
	"WRGSBASE",
	"WRMSR",
	"XADD",
	"XCHG",
	"XLAT",
	"XTLAB",
	"XOR"
};

void (*X86_64_64_BIT_INSTRUCTION_FUNCTIONS[])(struct ASSEMBLER_line*, uint64_t) = {
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	X86_64_64_BIT_INSTRUCTION_FUNCTION_MOV,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	X86_64_64_BIT_INSTRUCTION_FUNCTION_NOP,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

const uint8_t X86_64_64_BIT_REGISTER_COUNT = 16;
const char *X86_64_64_BIT_REGISTERS[] = {
	"RAX",
	"RCX",
	"RDX",
	"RBX",
	"RSP",
	"RBP",
	"RSI",
	"RDI",
	"R8",
	"R9",
	"R10",
	"R11",
	"R12",
	"R13",
	"R14",
	"R15"
};



// "main"

void assemble_line_x86_64_64_bit(struct ASSEMBLER_line *line){
	static uint64_t symbols_counter;
	static struct X86_64_64_BIT_line_extra_data meta;
	static unsigned char* value;

	memset(&meta, 0,  sizeof(struct X86_64_64_BIT_line_extra_data));
	for(symbols_counter=0; symbols_counter<line->symbols_count; symbols_counter++){
		// Parsing prefixes
		if(!strcasecmp("FS", line->symbols[symbols_counter].text)){
			if(meta.segment_override)
				ASSEMBLER_generate_error(line, ERROR_ERROR, "x86_64 - 64 bit - Multiple segment override preixes", 0, 0);

			value = line->symbols[symbols_counter].value;
			value[0] = 0x64;
			line->symbols[symbols_counter].value_length = 1;
			meta.segment_override = 1;
			continue;
		}else if(!strcasecmp("GS", line->symbols[symbols_counter].text)){
			if(meta.segment_override)
				ASSEMBLER_generate_error(line, ERROR_ERROR, "x86_64 - 64 bit - Multiple segment override preixes", 0, 0);

			value = line->symbols[symbols_counter].value;
			value[0] = 0x65;
			line->symbols[symbols_counter].value_length = 1;
			meta.segment_override = 1;
			continue;
		}else if(!strcasecmp("LOCK", line->symbols[symbols_counter].text)){
			if(meta.lock)
				ASSEMBLER_generate_error(line, ERROR_ERROR, "x86_64 - 64 bit - Multiple lock preixes", 0, 0);

			value = line->symbols[symbols_counter].value;
			value[0] = 0xF0;
			line->symbols[symbols_counter].value_length = 1;
			meta.lock = 1;
			continue;
		}else if(!strcasecmp("REP", line->symbols[symbols_counter].text) || !strcasecmp("REPE", line->symbols[symbols_counter].text) || !strcasecmp("REPZ", line->symbols[symbols_counter].text)){
			if(meta.repeat)
				ASSEMBLER_generate_error(line, ERROR_ERROR, "x86_64 - 64 bit - Multiple repeat preixes", 0, 0);

			value = line->symbols[symbols_counter].value;
			value[0] = 0xF3;
			line->symbols[symbols_counter].value_length = 1;
			meta.repeat = 1;
			continue;
		}else if(!strcasecmp("RENPE", line->symbols[symbols_counter].text) || !strcasecmp("REPNZ", line->symbols[symbols_counter].text)){
			if(meta.repeat)
				ASSEMBLER_generate_error(line, ERROR_ERROR, "x86_64 - 64 bit - Multiple repeat preixes", 0, 0);

			value = line->symbols[symbols_counter].value;
			value[0] = 0xF2;
			line->symbols[symbols_counter].value_length = 1;
			meta.repeat = 1;
			continue;
		}

		// Parsing instruction
		static uint16_t instructions_counter;
		for(instructions_counter=0; instructions_counter<X86_64_64_BIT_INSTRUCTIONS_COUNT; instructions_counter++)
			if(!strcasecmp(X86_64_64_BIT_INSTRUCTIONS[instructions_counter], line->symbols[symbols_counter].text)){
				if(X86_64_64_BIT_INSTRUCTION_FUNCTIONS[instructions_counter] != NULL)
					X86_64_64_BIT_INSTRUCTION_FUNCTIONS[instructions_counter](line, symbols_counter);
				else{
					line->extra_data = &meta;
					ASSEMBLER_generate_error(line, ERROR_ERROR, "x86_64 - 64 bit - Instruction not yet implemented", 0, 0);
				}

				return;
			}

		ASSEMBLER_generate_error(line, ERROR_ERROR, "x86_64 - 64 bit - Instruction not found", 0, 0);
		return;
	}
}

/*
 	uint8_t buffer[15], buffer_start = 0, buffer_length = 0;
	switch(globals.line.instruction){
		case NOP:
			buffer[0] = 0x90;
			buffer_length = 1;
		break;

		case INT:
			if(globals.line.operand1.size != 1){
				print_error("Operand must be 1 byte in length");
				return;
			}

			buffer[0] = 0xCD;
			buffer[1] = globals.line.operand1.value;
			buffer_length = 2;
		break;

		case IRET:
			buffer[0] = 0xCF;
			buffer_length = 1;
		break;

		case XLAT:
			buffer[0] = 0xD7;
			buffer_length = 1;
		break;

		case HLT:
			buffer[0] = 0xF4;
			buffer_length = 1;
		break;

		case CMC:
			buffer[0] = 0xF5;
			buffer_length = 1;
		break;

		case CLC:
			buffer[0] = 0xF8;
			buffer_length = 1;
		break;

		case STC:
			buffer[0] = 0xF9;
			buffer_length = 1;
		break;

		case CLI:
			buffer[0] = 0xFA;
			buffer_length = 1;
		break;

		case STI:
			buffer[0] = 0xFB;
			buffer_length = 1;
		break;

		case CLD:
			buffer[0] = 0xFC;
			buffer_length = 1;
		break;

		case STD:
			buffer[0] = 0xFD;
			buffer_length = 1;
		break;

		case MOV:
			if(globals.line.operand1.type != OPERAND_REGISTER){
				print_error("First operand for MOV must be a register");
				return;
			}

			if(globals.line.operand2.size > globals.line.operand1.size){
				print_error("Operand 2 must be <= operand 1");
				return;
			}

			if(globals.line.operand2.type == OPERAND_REGISTER){
				if(globals.line.operand2.size != globals.line.operand1.size){
					print_error("Registers must be same size");
					return;
				}

				if(globals.line.operand1.size == 1){
					buffer[0] = 0x88;
					buffer_length = 1;
				}else if(globals.line.operand1.size == 2){
					buffer[0] = 0x66;
					buffer[1] = 0x89;
					buffer_length = 2;
				}else if(globals.line.operand1.size == 4){
					buffer[0] = 0x89;
					buffer_length = 1;
				}else if(globals.line.operand1.size == 8){
					buffer[0] = 0x48;
					buffer[1] = 0x89;
					buffer_length = 2;

					if(globals.line.operand1.value >= R8)
						buffer[0]++;

					if(globals.line.operand2.value >= R8)
						buffer[0] += 4;
				}

				buffer[buffer_length] = ((globals.line.operand2.value % 8) << 3) + (globals.line.operand1.value % 8) + 0xC0;
				buffer_length++;

				break;
			}

			switch(globals.line.operand1.size){
				case 1:
					if(globals.line.operand2.size != 1){
						print_error("Operand 2 must be 1 byte");
						return;
					}

					buffer[0] = 0xB0 + globals.line.operand1.value;
					buffer[1] = globals.line.operand2.value;
					buffer_length = 2;
				break;
				case 2:
					buffer[0] = 0x66;
					buffer_start = 1;
					goto mov_instruction_assemble;
				case 8:
					buffer[0] = 0x48;
					buffer_start = 1;

					if(globals.line.operand1.value >= R8){
						buffer[0]++;
						globals.line.operand1.value %= 8;
					}
					// fallthrough
				case 4:
					mov_instruction_assemble:
					buffer_length = buffer_start + 1 + globals.line.operand1.size;
					buffer[buffer_start] = 0xB8 + globals.line.operand1.value;
					memcpy(&buffer[buffer_start+1], &globals.line.operand2.value, globals.line.operand2.size);
				break;
				default:
					print_error("Invalid operand size");
					return;
				break;
			}
		break;

		default:
			return;
	}

	printf("%"PRIu64": %"PRIu8"\n", globals.line_number, buffer_length);
	if(buffer_length)
		fwrite(&buffer, 1, buffer_length, globals.out);
*/
