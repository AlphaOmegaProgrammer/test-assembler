#ifndef ARCHITECTURE_x86_64_INCLUDED
#define ARCHITECTURE_x86_64_INCLUDED

void x86_64_cpu_mode_directive_handler(struct ASSEMBLER_line*);
void x86_64_line_handler(struct ASSEMBLER_line*);

const struct ASSEMBLER_generic_callbacks X86_64_DIRECTIVES[] = {
	{"CPU_MODE", x86_64_cpu_mode_directive_handler}
};

#endif /* ARCHITECTURE_x86_64_INCLUDED */
