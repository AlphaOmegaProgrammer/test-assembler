#ifndef ASSEMLBER_H_INCLUDED
#define ASSEMLBER_H_INCLUDED

#include <inttypes.h>
#include <stdio.h>

#include "assembler-configuration-defines.h"

enum ASSEMBLER_error_types{
	ERROR_WARNING,
	ERROR_ERROR
};

struct ASSEMBLER_symbol{
	char *text;
	void *value;
	uint64_t value_length;
};



struct ASSEMBLER_error{
	enum ASSEMBLER_error_types error_type;
	char *error_string;
	uint64_t start, end;
	struct ASSEMBLER_error *next;
};



struct ASSEMBLER_line{
	uint64_t line_number, symbols_count;
	char *line_string;
	struct ASSEMBLER_symbol *symbols;
	struct ASSEMBLER_error *errors;
	struct ASSEMBLER_line *next;
	void* extra_data;
};



struct ASSEMBLER_output_format_callbacks{
	char *text;
	unsigned char (*line_handler)(struct ASSEMBLER_line*, FILE*);
};

struct ASSEMBLER_generic_callbacks{
	char *text;
	void (*line_handler)(struct ASSEMBLER_line*);
};



struct ASSEMBLER_architecture_callbacks{
	char *text;
	void (*line_handler)(struct ASSEMBLER_line*);
	uint64_t directives_count;
	struct ASSEMBLER_generic_callbacks *directives;
};



void ASSEMBLER_generate_error(struct ASSEMBLER_line*, enum ASSEMBLER_error_types, char*, uint64_t, uint64_t);
void ASSEMBLER_print_errors(struct ASSEMBLER_line*);
void ASEEMBLER_parse_literal(struct ASSEMBLER_line*, uint64_t);

#endif /* ASSEMLBER_H_INCLUDED */

