#include <inttypes.h>
#include <stdio.h>
#include <unistd.h>

#include "assembler.h"

unsigned char flat_binary_line_handler(struct ASSEMBLER_line* lines_root, FILE* out){
	static struct ASSEMBLER_line *line;
	for(line = lines_root; line != NULL; line = line->next){
		static uint64_t counter;
		for(counter=0; counter<line->symbols_count; counter++)
			if(fwrite(line->symbols[counter].value, 1, line->symbols[counter].value_length, out) != line->symbols[counter].value_length)
				return 0;
	}

	return 1;
}
