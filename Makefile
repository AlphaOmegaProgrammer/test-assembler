MAKE_ROOT := $(dir $(abspath $(firstword $(MAKEFILE)LIST)))

CC = gcc -I$(MAKE_ROOT)src
CFLAGS = -O3 -march=native -xc -std=c11 -fstack-protector-all -Wall -Wextra -pedantic
CFLAGS_DEBUG = $(CFLAGS) -g -O0

export



all: clean assembler architectures output_formats final
debug: clean assembler_debug architectures_debug output_formats_debug final



final:
	$(CC) objs/assembler.o objs/*.a -o bin/assembler



assembler:
	$(CC) $(CFLAGS) -c src/assembler-main.c -o objs/assembler.o

assembler_debug:
	$(CC) $(CFLAGS_DEBUG) -c src/assembler-main.c -o objs/assembler.o



architectures:
	$(MAKE) -C src/architectures all

architectures_debug:
	$(MAKE) -C src/architectures debug



output_formats:
	$(MAKE) -C src/output-formats all

output_formats_debug:
	$(MAKE) -C src/output-formats debug



clean:
	rm -Rf bin objs/*.a objs/*/*.a objs/*/*/*.a oobjs/*/*/*/*.a bjs/*.o objs/*/*.o objs/*/*/*.o objs/*/*/*/*.o
	mkdir bin

distclean:
	rm -Rf {bin,objs,tests/bin}
	mkdir bin objs/{,architectures/{,x86_64/{,64_bit}},output_formats} tests/bin

test:
	./build_test.sh
