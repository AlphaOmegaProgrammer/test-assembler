mov al, al
mov al, ah
mov al, cl
mov al, ch
mov al, dl
mov al, dh
mov al, bl
mov al, bh

mov ah, al
mov ah, ah
mov ah, cl
mov ah, ch
mov ah, dl
mov ah, dh
mov ah, bl
mov ah, bh

mov cl, al
mov cl, ah
mov cl, cl
mov cl, ch
mov cl, dl
mov cl, dh
mov cl, bl
mov cl, bh

mov ch, al
mov ch, ah
mov ch, cl
mov ch, ch
mov ch, dl
mov ch, dh
mov ch, bl
mov ch, bh

mov dl, al
mov dl, ah
mov dl, cl
mov dl, ch
mov dl, dl
mov dl, dh
mov dl, bl
mov dl, bh

mov dh, al
mov dh, ah
mov dh, cl
mov dh, ch
mov dh, dl
mov dh, dh
mov dh, bl
mov dh, bh

mov bl, al
mov bl, ah
mov bl, cl
mov bl, ch
mov bl, dl
mov bl, dh
mov bl, bl
mov bl, bh

mov bh, al
mov bh, ah
mov bh, cl
mov bh, ch
mov bh, dl
mov bh, dh
mov bh, bl
mov bh, bh
