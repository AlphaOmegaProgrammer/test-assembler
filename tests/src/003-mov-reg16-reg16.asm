mov ax, ax
mov ax, cx
mov ax, dx
mov ax, bx
mov ax, sp
mov ax, bp
mov ax, si
mov ax, di

mov cx, ax
mov cx, cx
mov cx, dx
mov cx, bx
mov cx, sp
mov cx, bp
mov cx, si
mov cx, di

mov dx, ax
mov dx, cx
mov dx, dx
mov dx, bx
mov dx, sp
mov dx, bp
mov dx, si
mov dx, di

mov bx, ax
mov bx, cx
mov bx, dx
mov bx, bx
mov bx, sp
mov bx, bp
mov bx, si
mov bx, di

mov sp, ax
mov sp, cx
mov sp, dx
mov sp, bx
mov sp, sp
mov sp, bp
mov sp, si
mov sp, di

mov bp, ax
mov bp, cx
mov bp, dx
mov bp, bx
mov bp, sp
mov bp, bp
mov bp, si
mov bp, di

mov si, ax
mov si, cx
mov si, dx
mov si, bx
mov si, sp
mov si, bp
mov si, si
mov si, di

mov di, ax
mov di, cx
mov di, dx
mov di, bx
mov di, sp
mov di, bp
mov di, si
mov di, di
